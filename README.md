# alphaGraph
A tool for exploring graphic letter form.

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---

![anna_print_01.jpg](https://bitbucket.org/repo/rpkRnBy/images/3025243167-anna_print_01.jpg)

## Presentation

Exploring letter forms in a parametric manner. alphaGraph is a small parametric tool developed in Processing for exploring letter forms. It chooses a font from your machine and translates this into a grid on which two basic geometric forms are displayed: point and line. Using a custom gui, parameters can easily be modified on the fly to create a wide range of graphic results. Letters can be worked on individually or as entire words, using the keyboard for input. Images can be exported as vector graphics. alphaGraph is the first in a series of tools that explore transdisciplinary approaches to working with graphic form. Custom-made software, it's conception, development and their applications lie at the heart of this project. It is my intention to develop on these reflections with and through a series of workshops and projects that specifically take shape within the context of tool-making.
"More tool-making, less tool-demonstrating."

## Context

This project comes from a whole mix of thoughts on tool making. I wanted to make a simple tool for quick prototyping that lets graphic designers explore letter forms without getting their hands dirty in the coding part. As a first experiment, the idea was to provide a tool for a number of graphic design students and see what they did with it. From here, only time will tell the outcomes that emerge. Read more about these experiments in projects and workshops.

## Projects & Workshops

* [Parametric](https://parametrictype.bitbucket.io)
	Parametric is a research website documenting a workshop with III year BA graphic design students from ESAC Cambrai Art School, France.
* [Lettrines](http://workshop-lettrine-01.esad-amiens.fr/)
	Lettrines is a website documenting a workshop with IV year Master students from ESAD Amiens Art School, France.
* [Lettres ornées](https://www.flickr.com/photos/tisane_01/32104109192/in/album-72157626131513583/)
	Lettres ornées is a workshop with III year BA art students from ESAL Metz Art School, France.
* [Article: personal website](https://area03.bitbucket.io/projects/alphaGraph.html)
* [Presse++]()
	Presse++ is a research project yet to be finished. WIP

* [Photos](https://www.flickr.com/photos/tisane_01/32104109192/in/album-72157626131513583/)


## Install

The programs in this repository must be compiled using the Processing environment.

https://processing.org/

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.95
* Tools used : Processing

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
