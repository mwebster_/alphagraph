/*
 :::::::::::::::::::::::::::::::
 ------------------------------
 PROJECT : ALPHAGRPAH 2018  
 ------------------------------
 ::::::::::::::::::::::::::::::
 
 DEV 17/12/2018
 Sketch : alphaGraph 
 
 - This software helps in generating ideas for letter forms ;–)
 
 - GIT REPO : https://bitbucket.org/mwebster_/alphagraph
 - WORKSHOPS: https://parametrictype.bitbucket.io
 - WORKSHOPS: http://workshop-lettrine-01.esad-amiens.fr/
 - More info: https://area03.bitbucket.io/projects/alphaGraph.html
 
 NOTES TO SELF :
 
 
 
 *******************************************************
 * LETTERS & FORM is a series of software that explores 
 graphical possibilites with letter forms.
 *******************************************************
 Written by MW2015
 https://area03.bitbucket.io
 
 INSTRUCTIONS
 ------------
 - Type text and play with various parameters.
 - MousePressed when "MOUSE" button has been activated deforms text. 
 The deform function is wired to mouse x & y position creating different effects accordingly.
 - ENTER > Clear screen & create a new grid.
 - exportPDF button exports to PDF.
 - SAVE & LOAD buttons enable one to save and load presets as separate files.
 
 ///////////////////////////// END
 
 
 * Copyright (c) 2015 Mark Webster
 * 
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * https://www.gnu.org/licenses/gpl-3.0.en.html
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 
 
 */